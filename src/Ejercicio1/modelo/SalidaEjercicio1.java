package Ejercicio1.modelo;

import Utilidades.SalidaDeEjercicio;

/**
 * Created by Tomi on 06/04/2014.
 */
public class SalidaEjercicio1 implements SalidaDeEjercicio {

    private final int mejorDiaDeContratacion;
    private final int cantidadDeCamionesInspeccionados;

    public SalidaEjercicio1(int mejorDiaDeContratacion, int cantidadDeCamionesInspeccionados) {
        this.mejorDiaDeContratacion = mejorDiaDeContratacion;
        this.cantidadDeCamionesInspeccionados = cantidadDeCamionesInspeccionados;
    }

    public void imprimir() {
        System.out.println(mejorDiaDeContratacion + " " + cantidadDeCamionesInspeccionados);
    }
}
