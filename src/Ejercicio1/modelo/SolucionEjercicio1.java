package Ejercicio1.modelo;

import Utilidades.Solucion;

/**
 * Created by Tomi on 06/04/2014.
 */
public class SolucionEjercicio1 implements Solucion {

    private final EntradaEjercicio1 entrada;

    public SolucionEjercicio1(EntradaEjercicio1 entrada) {
        this.entrada = entrada;
    }

    public SalidaEjercicio1 solucionOptima() {
        int mayorCantidadDeDiasInspeccionados = 0;
        int mejorDiaDeContratacion = 0;
        int[] diasDeInspeccion = entrada.getDiasDeLlegadaDeCamiones();
        Utils.mergeSort(diasDeInspeccion);

        for ( int indexDia = 0; indexDia < diasDeInspeccion.length; indexDia++ ) {
            int dia = diasDeInspeccion[ indexDia ];

            // Si es un día repetido no lo examino.
            if ( indexDia > 0 && dia == diasDeInspeccion[ indexDia - 1 ] )
                continue;

            // Si la mayor cantidad de camiones inspeccionados hasta el momento es mayor o igual a
            // la cantidad de camiones que quedan por inspeccionar, termino la busqueda ya que no
            // hay forma de que sea superada.
            if ( mayorCantidadDeDiasInspeccionados >= diasDeInspeccion.length - indexDia )
                break;

            int inspeccionados = inspeccionados(diasDeInspeccion, indexDia, entrada.getDiasDeContratacionDelExperto());

            if ( inspeccionados > mayorCantidadDeDiasInspeccionados ) {
                mejorDiaDeContratacion = diasDeInspeccion[indexDia];
                mayorCantidadDeDiasInspeccionados = inspeccionados;
            }
        }
        return new SalidaEjercicio1(mejorDiaDeContratacion, mayorCantidadDeDiasInspeccionados);
    }

    // Requiere un arreglo ordenado
    private int inspeccionados(int[] diasDeInspeccion, int indexDia, int diasDeContratacionDelExperto) {
        int indicePrimerDiaNoInspeccionado = indiceDelPrimerDiaNoInspeccionado(diasDeInspeccion, diasDeInspeccion[indexDia] + diasDeContratacionDelExperto);
        return indicePrimerDiaNoInspeccionado - indexDia;
    }

    private int indiceDelPrimerDiaNoInspeccionado(int[] array, int elem) {
        int first = 0, last = array.length - 1;
        int elemIndex = -1;

        while ( first <= last ) {
            int mid = (first + last) / 2;
            if ( array[ mid ] < elem )
                first = mid + 1;
            else if ( array[ mid ] > elem )
                last = mid - 1;
            else if ( array[ mid ] == elem ) {
                elemIndex = mid;
                last = mid - 1;
            }
        }

        if ( elemIndex == -1 )
            return first;
        return elemIndex;
    }
}
