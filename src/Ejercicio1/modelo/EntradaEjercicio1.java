package Ejercicio1.modelo;

import java.util.Arrays;

/**
 * Created by Tomi on 06/04/2014.
 */
public class EntradaEjercicio1 {

    private final int[] diasDeLlegadaDeCamiones;
    private final int diasDeContratacionDelExperto;

    public EntradaEjercicio1(String entrada) throws Exception {
        String[] valores = entrada.split(" ");
        diasDeContratacionDelExperto = Integer.valueOf(valores[0]);
        if(diasDeContratacionDelExperto <= 0)
            throw new Exception("Dias de contratacion del experto invalido.");

        int cantidadDeCamiones = Integer.valueOf(valores[1]);
        if(valores.length != cantidadDeCamiones + 2)
            throw new Exception("Entrada invalida.");

        this.diasDeLlegadaDeCamiones = new int[cantidadDeCamiones];
        for(int i = 0; i < cantidadDeCamiones; i++){
            diasDeLlegadaDeCamiones[i] = Integer.valueOf(valores[i+2]);
            if(diasDeLlegadaDeCamiones[i] <= 0)
                throw new Exception("El dia " + diasDeLlegadaDeCamiones[i] + " es invalido.");
        }
    }

    public int[] getDiasDeLlegadaDeCamiones() {
        return diasDeLlegadaDeCamiones;
    }

    public int getDiasDeContratacionDelExperto() {
        return diasDeContratacionDelExperto;
    }

    @Override
    public String toString() {
        return "EntradaEjercicio2{" +
                "diasDeLlegadaDeCamiones=" + Arrays.toString(diasDeLlegadaDeCamiones) +
                ", diasDeContratacionDelExperto=" + diasDeContratacionDelExperto +
                '}';
    }
}
