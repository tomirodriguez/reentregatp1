package Ejercicio1.main;

import Ejercicio1.modelo.EntradaEjercicio1;
import Ejercicio1.modelo.SalidaEjercicio1;
import Ejercicio1.modelo.SolucionEjercicio1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomi on 06/04/2014.
 */
public class MainEjercicio1 {

    public static void main( String[] args ) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        List<EntradaEjercicio1> entradas = new ArrayList<EntradaEjercicio1>();

        System.out.println("Problemas: ");
        String input = br.readLine();
        while(!input.equals("0")) {
            entradas.add(new EntradaEjercicio1(input));
            input = br.readLine();
        }

        System.out.println("Soluciones: ");
        for(EntradaEjercicio1 entrada : entradas){
            SolucionEjercicio1 solucion = new SolucionEjercicio1(entrada);
            SalidaEjercicio1 salida = solucion.solucionOptima();
            salida.imprimir();
        }
    }
}
