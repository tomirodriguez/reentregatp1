package Ejercicio2.modelo;

/**
 * Created by Tomi on 06/04/2014.
 */
public class EntradaEjercicio2 {

    private final Joya[] joyas;

    public EntradaEjercicio2(String entrada) throws Exception {
        String[] valores = entrada.split("\n");
        int cantidadDeJoyas = Integer.valueOf(valores[0]);
        joyas = new Joya[cantidadDeJoyas];
        for(int i = 1; i <= cantidadDeJoyas; i++){
            String[] datosJoya = valores[i].split(" ");
            joyas[i-1] = new Joya(Integer.valueOf(datosJoya[0]), Integer.valueOf(datosJoya[1]), i);
        }
    }

    public Joya[] getJoyas() {
        return joyas;
    }
}
