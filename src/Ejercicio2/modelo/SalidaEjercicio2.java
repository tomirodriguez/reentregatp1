package Ejercicio2.modelo;

import Utilidades.SalidaDeEjercicio;

/**
 * Created by Tomi on 06/04/2014.
 */
public class SalidaEjercicio2 implements SalidaDeEjercicio {

    private final Joya[] joyas;
    private final int perdidaTotal;

    public SalidaEjercicio2(Joya[] joyas, int perdidaTotal) {
        this.joyas = joyas;
        this.perdidaTotal = perdidaTotal;
    }

    public void imprimir() {
        for(Joya joya : joyas)
            System.out.print(joya.getNumeroDeJoya() + " ");
        System.out.println(perdidaTotal);
    }
}
