package Ejercicio2.modelo;

import Utilidades.Solucion;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Tomi on 06/04/2014.
 */
public class SolucionEjercicio2 implements Solucion {

    private final EntradaEjercicio2 entrada;

    public SolucionEjercicio2(EntradaEjercicio2 entrada) {
        this.entrada = entrada;
    }

    public SalidaEjercicio2 solucionOptima() {
        Joya[] joyas = entrada.getJoyas();
        Arrays.sort(entrada.getJoyas(), new Comparator<Joya>() {
            @Override
            public int compare(Joya o1, Joya o2) {
                double coeficienteO1 = Double.valueOf(o1.getPerdidaDiariaDeGanancia()) / Double.valueOf(o1.getTiempoDeFabricacion());
                double coeficienteO2 = Double.valueOf(o2.getPerdidaDiariaDeGanancia()) / Double.valueOf(o2.getTiempoDeFabricacion());
                return -Double.compare(coeficienteO1,coeficienteO2);
            }
        });

        int perdidas = 0;
        int dias = 0;
        for(Joya joya : joyas){
            dias += joya.getTiempoDeFabricacion();
            perdidas += dias * joya.getPerdidaDiariaDeGanancia();
        }
        return new SalidaEjercicio2(joyas, perdidas);
    }

}
