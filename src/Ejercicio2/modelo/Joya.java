package Ejercicio2.modelo;

/**
 * Created by Tomi on 15-Apr-14.
 */
public class Joya {

    private final int perdidaDiariaDeGanancia;
    private final int tiempoDeFabricacion;
    private final int numeroDeJoya;

    public Joya(int perdidaDiariaDeGanancia, int tiempoDeFabricacion, int index) throws Exception {
        if(perdidaDiariaDeGanancia < 0 || tiempoDeFabricacion < 0)
            throw new Exception("Joya invalida. La perdida diaria de ganancia y el tiempo de fabricacion deben ser positivos.");

        this.numeroDeJoya = index;
        this.perdidaDiariaDeGanancia = perdidaDiariaDeGanancia;
        this.tiempoDeFabricacion = tiempoDeFabricacion;
    }

    public int getPerdidaDiariaDeGanancia() {
        return perdidaDiariaDeGanancia;
    }

    public int getTiempoDeFabricacion() {
        return tiempoDeFabricacion;
    }

    public int getNumeroDeJoya() {
        return numeroDeJoya;
    }

    @Override
    public String toString() {
        return "Joya{" +
                "perdidaDiariaDeGanancia=" + perdidaDiariaDeGanancia +
                ", tiempoDeFabricacion=" + tiempoDeFabricacion +
                ", numeroDeJoya=" + numeroDeJoya +
                '}';
    }
}
