package Ejercicio2.main;

import Ejercicio2.modelo.EntradaEjercicio2;
import Ejercicio2.modelo.SalidaEjercicio2;
import Ejercicio2.modelo.SolucionEjercicio2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomi on 06/04/2014.
 */
public class MainEjercicio2 {

    public static void main( String[] args ) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        List<EntradaEjercicio2> entradas = new ArrayList<EntradaEjercicio2>();

        System.out.println("Problemas: ");
        String input = br.readLine();
        while(!input.equals("0")) {
            int cantidadDeJoyas = Integer.parseInt(input);
            for(int i = 1; i <= cantidadDeJoyas; i++){
                input += "\n" + br.readLine();
            }
            entradas.add(new EntradaEjercicio2(input));
            input = br.readLine();
        }

        System.out.println("Soluciones: ");
        for(EntradaEjercicio2 entrada : entradas){
            SolucionEjercicio2 solucion = new SolucionEjercicio2(entrada);
            SalidaEjercicio2 salida = solucion.solucionOptima();
            salida.imprimir();
        }
    }
}
